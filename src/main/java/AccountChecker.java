public class AccountChecker {
    public static void main(String[] args) {
        BharatBank Gopal = new BharatBank(50000, "Current");
        BharatBank Amrita = new BharatBank(100000, "Savings");

        // On Jan 1st
        Gopal.deposit(10000);

        double interest;
        interest= Gopal.getInterest();
        System.out.println("Gopal's balance and interest in Jan-Mar quarter: " + Gopal.getBalance()+" and "+interest);
        interest= Amrita.getInterest();
        System.out.println("Amrita's balance and interest in Jan-Mar quarter: " + Amrita.getBalance()+" and "+interest);

        // On March 1st
        Amrita.withdraw(45000);
        interest= Amrita.getInterest();
        System.out.println("Amrita's balance and interest in Jan-Mar quarter: " + Amrita.getBalance()+" and "+interest);

        // Today
        Gopal.withdraw(70000);
        System.out.println("Gopal's balance: "+Gopal.getBalance());

    }
}
