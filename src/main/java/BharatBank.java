public class BharatBank {
    private double balance;
    private final String accountType;

    BharatBank(double balance, String accountType){
        this.balance=  balance;
        this.accountType= accountType;
    }

    public double getBalance(){
        return this.balance;
    }

    public String getAccountType(){
        return this.accountType;
    }

    public void deposit(double amount){
        this.balance += amount;
    }

    public void withdraw(double amount){
        if(getAccountType().equalsIgnoreCase("Savings")){
            if(getBalance()<amount)
                System.out.println("Insufficient balance to withdraw!!");
            else
                this.balance -= amount;
        }
        else{
            if(getBalance()<=amount)
                this.balance -= amount;
            else {
                double overdraft_limit, excess_amount;
                overdraft_limit = 0.2 * getBalance();
                excess_amount= amount-getBalance();
                if(overdraft_limit>=excess_amount)
                    this.balance= -excess_amount;
            }
        }
    }

    public double getInterest(){
        double rate = 0;
        if(getAccountType().equalsIgnoreCase("Savings"))
            rate= 3;
        return getBalance() * rate * 0.25;
    }

}
